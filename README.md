# README #

* Multilingual
* Translate survey, data entry form
* Switch between languages on the fly
* Add translations via Online Designer
* Project specific languages
* Tested with REDCap 7.2.2 and 7.4.3

### Setup ###
* clone repository to redcap/plugins/
* copy 3 files in redcap/plugins/multilingual/hookFiles to redcap/hooks/pidXXXX/

### Author ###
* Children's Mercy Hospital
* Kansas City, MO
* Steve Martin, samartin1@cmh.edu