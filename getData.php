<?php
define("NOAUTH", false);
include '../../redcap_connect.php';

/**********************/
/***COMMON FUNCTIONS***/
/**********************/

/**************************/
/***END COMMON FUNCTIONS***/
/**************************/

$data = @$_POST['data'];

if(isset($data) && $data != ''){
	$data = json_decode($data, true);
	
	switch($data['todo']){
		case 1:
			getTranslations($data);
			break;
		case 2:
			getAnswers($data);
			break;
		default:
			exit;
	}
}
else{
	header("HTTP/1.0 404 Not Found");
}

function getAnswers($data){
	global $conn;
	
	if($data['matrix'] == 1){
		$query = "SELECT element_enum, element_type, element_validation_type FROM redcap_metadata
			WHERE project_id = " . $data['project_id'] . " 
			AND grid_name LIKE '" . $data['field_name'] . "'
			LIMIT 1";
	}
	else{
		$query = "SELECT element_enum, element_type, element_validation_type FROM redcap_metadata
			WHERE project_id = " . $data['project_id'] . " 
			AND field_name LIKE '" . $data['field_name'] . "'";
	}
	$result = mysqli_query($conn, $query);
	
	$row = mysqli_fetch_array($result);
		
	$tmp = explode(' \n ', $row['element_enum']);
	foreach($tmp AS $key => $value){
		$tmp2 = explode(',', $value);
		$response[trim($tmp2[0])] = trim($tmp2[1]);
	}
	
	if($row['element_type'] == 'text' && strpos($row['element_validation_type'], 'date') !== false){
		$response = null;
		$response['0'] = 'Answer';
	}
	elseif($row['element_type'] == 'file' && strpos($row['element_validation_type'], 'signature') !== false){
		$response = null;
		$response['0'] = 'Answer';
	}
	elseif($row['element_type'] == 'file' && $row['element_validation_type'] == null){
		$response = null;
		$response['0'] = 'Answer';
	}
	elseif($row['element_type'] == 'calc'){
		$response = null;
		$response[""] = "";
	}
	elseif($row['element_type'] == 'yesno'){
		$response = null;
		$response['0'] = "No";
		$response['1'] = "Yes";
	}
	elseif($row['element_type'] == 'truefalse'){
		$response = null;
		$response['0'] = "False";
		$response['1'] = "True";
	}
	
	header('Content-Type: application/json');
	echo json_encode($response);
}

function getTranslations($data){
	global $conn;
	
	$query = "SELECT field_name, element_type, misc, grid_name, element_validation_type, element_label FROM redcap_metadata 
		WHERE project_id = " . $data['project_id'] . " 
			AND form_name LIKE '" . $data['page'] . "'";
	$result = mysqli_query($conn, $query);

	while($row = mysqli_fetch_array($result)){
		//default questions
		$response['defaults'][$row['field_name']] = strip_tags($row['element_label']);
		
		$misc = explode(PHP_EOL, $row['misc']);
		foreach($misc AS $key => $value){
			//questions
			if(strpos($value, '@p1000lang') !== false){
				$value = str_replace('@p1000lang', '', $value);
				$value = json_decode($value, true);
				foreach($value AS $key2 => $trans){
					if($key2 == $data['lang']){
						$response['questions'][$row['field_name']]['text'] = $trans;
						if(strpos($row['element_validation_type'], 'date') !== false){
							$response['questions'][$row['field_name']]['type'] = 'date';
						}
						else{
							$response['questions'][$row['field_name']]['type'] = $row['element_type'];
						}
						$response['questions'][$row['field_name']]['matrix'] = $row['grid_name'];
					}
				}
			}
			//answers
			elseif(strpos($value, '@p1000answers') !== false){
				$value = str_replace('@p1000answers', '', $value);
				$value = json_decode($value, true);
				foreach($value AS $key2 => $trans){
					if($key2 == $data['lang']){
						$response['answers'][$row['field_name']]['text'] = $trans;
						if(strpos($row['element_validation_type'], 'date') !== false){
							$response['answers'][$row['field_name']]['type'] = 'date';
						}
						elseif(strpos($row['element_validation_type'], 'signature') !== false){
							$response['answers'][$row['field_name']]['type'] = 'signature';
						}
						else{
							$response['answers'][$row['field_name']]['type'] = $row['element_type'];
						}
						$response['answers'][$row['field_name']]['matrix'] = $row['grid_name'];
					}
				}
			}
			//errors
			if(strpos($value, '@p1000errors') !== false){
				$value = str_replace('@p1000errors', '', $value);
				$value = json_decode($value, true);
				foreach($value AS $key2 => $trans){
					if($key2 == $data['lang']){
						$response['errors'][$row['field_name']]['text'] = $trans;
						if(strpos($row['element_validation_type'], 'date') !== false){
							$response['errors'][$row['field_name']]['type'] = 'date';
						}
						else{
							$response['errors'][$row['field_name']]['type'] = $row['element_type'];
						}
						$response['errors'][$row['field_name']]['matrix'] = $row['grid_name'];
					}
				}
			}
		}
	}
	
	header('Content-Type: application/json');
	echo json_encode($response);
}

?>
